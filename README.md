# TP6文件存储库 - 七牛云驱动

> 基于 `chleniang/filesystem` 的**七牛云驱动**

## 需求

  - php >= 8.0.2 ( league/flysystem ^3.0的要求 )
  - ThinkPHP >= 6.0 ( 建议使用6.1 移除了TP framework自带的filesystem )
  - chleniang/filesystem

## 安装

`composer require "chleniang/filesystem-qiniu"`

> 本驱动依赖库 `overtrue/flysystem-qiniu` `^3.0`

  [> 七牛官方文档 <](https://developer.qiniu.com/kodo/1241/php)

  [> overtrue/flysystem-qiniu 参考API <](https://github.com/overtrue/flysystem-qiniu)

   对于 `overtrue/flysystem-qiniu` 中 `adapter` 独有的API，通过 `getAdapter()` 后直接使用:

   示例 `Filesystem::disk('qiniu')->getAdapter()->getUrl('someFilePath.txt')`

## 使用

  ```php
  # config/filesystem.php 文件中, 'disks'配置项添加 'qiniu' 配置
    // 磁盘列表
    'disks'   => [
      ...,
      'qiniu_upload' => [  // 磁盘名称,可以任意取(不重复即可)
        'type' => 'qiniu',  // 驱动类型,必须是 'qiniu'
        'accessKey' => '', // 七牛云AccessKey
        'secretKey' => '', // 七牛云SecretKey
        'bucket' => '', // 七牛云存储区域
        'domain' => '',  // 空间绑定的域名
      ]
    ]
  
  # 使用
  \chleniang\filesystem\facade\Filesystem::disk('qiniu_upload')->[flysystemAPI...]
  // adapter独有API可直接调用,如果adapter中API与flysystemAPI重名,优先调用flysystemAPI
  \chleniang\filesystem\facade\Filesystem::disk('qiniu_upload')->[adapter独有API...];
  ```
   
## Lisence

  - [Mulan PSL v2](http://license.coscl.org.cn/MulanPSL2)

