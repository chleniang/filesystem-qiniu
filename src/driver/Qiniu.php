<?php
// +------------------------------------------------------------------
// | chleniang/filesystem
// | Copyright (c) 2022 All rights reserved.
// | Based on ThinkPHP6.1
// | Licensed MulanPSL2( http://license.coscl.org.cn/MulanPSL2 )
// | Author: CLS <422064377>
// | CreateDate: 2022/10/31
// +------------------------------------------------------------------

namespace chleniang\filesystem\driver;

use chleniang\filesystem\Driver;
use League\Flysystem\FilesystemAdapter;
use Overtrue\Flysystem\Qiniu\QiniuAdapter;

class Qiniu extends Driver
{
    /**
     * @var FilesystemAdapter
     */
    protected ?FilesystemAdapter $adapter = null;

    protected function createAdapter(): FilesystemAdapter
    {
        $this->adapter = new QiniuAdapter(
            $this->config['accessKey'],
            $this->config['secretKey'],
            $this->config['bucket'],
            $this->config['domain'],
        );
        return $this->adapter;
    }

    /**
     * 获取URL
     *      迎合TP原有方法名
     *
     * @param string $path
     *
     * @return string
     */
    public function url(string $path): string
    {
        return $this->adapter->getUrl($path);
    }
}